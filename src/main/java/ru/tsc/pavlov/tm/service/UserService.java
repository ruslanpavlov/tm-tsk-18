package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.repository.IUserRepository;
import ru.tsc.pavlov.tm.api.service.IUserService;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserEmailException;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserLoginException;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserPasswordException;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserRoleException;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.HashUtil;
import ru.tsc.pavlov.tm.util.StringUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(String login, String password) {
        if (StringUtil.isEmpty(login)) throw new EmptyUserLoginException();
        if (StringUtil.isEmpty(password)) throw new EmptyUserPasswordException(password);
        final User user = new User(login, HashUtil.salt(password), UserRole.USER);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(String login, String password, String email) {
        if (StringUtil.isEmpty(login)) throw new EmptyUserLoginException();
        if (StringUtil.isEmpty(password)) throw new EmptyUserPasswordException(password);
        if (StringUtil.isEmpty(email)) throw new EmptyUserEmailException(login);
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, UserRole role) {
        if (StringUtil.isEmpty(login)) throw new EmptyUserLoginException();
        if (StringUtil.isEmpty(password)) throw new EmptyUserPasswordException(password);
        if (StringUtil.isEmpty(role)) throw new EmptyUserRoleException(login);

        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User setRole(String id, UserRole role) {
        final User user = findById(id);
        user.setRole(role);
        return user;
    }

    @Override
    public void remove(User user) {
        userRepository.remove(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public User findById(final String id) {
        return userRepository.findById(id);
    }

    @Override
    public User findByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User setPassword(final String id, final String password) {
        final User user = findById(id);
        user.setPassword(HashUtil.salt(password));
        return user;
    }

    @Override
    public User removeById(final String id) {
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        return userRepository.removeByLogin(login);
    }

    @Override
    public User updateByLogin(String login, String lastName, String firstName, String middleName, String email) {
        final User user = userRepository.findByLogin(login);

        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);

        return user;

    }

}

