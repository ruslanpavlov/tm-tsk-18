package ru.tsc.pavlov.tm.api.service;

import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.enumerated.UserRole;

import java.util.List;

public interface IUserService {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, UserRole role);

    User setRole(String id, UserRole role);

    void remove(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);


    User findByEmail(String email);

    User setPassword(String id, String password);

    User removeById(String id);

    User removeByLogin(String login);

    User updateByLogin(String login, String lastName, String firstName, String middleName, String email);

}
