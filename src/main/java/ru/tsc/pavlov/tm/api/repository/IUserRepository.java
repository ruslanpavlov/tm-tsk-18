package ru.tsc.pavlov.tm.api.repository;

import ru.tsc.pavlov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    void add(User user);

    void remove(User user);

    void clear();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeById(String id);

}
