package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyUserEmailException extends AbstractException {

    public EmptyUserEmailException() {
        super("Error! Email is empty.");
    }

    public EmptyUserEmailException(String value) {
        super("Error! Email for User: " + value + " is empty.");
    }

}
