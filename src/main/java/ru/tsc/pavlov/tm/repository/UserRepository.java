package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.IUserRepository;
import ru.tsc.pavlov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public void add(final User user) {
        users.add(user);
    }

    @Override
    public void remove(final User user) {
        users.remove(user);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public User findById(String id) {
        for (User user : users)
            if (id.equals(user.getId())) return user;
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (User user : users)
            if (login.equals(user.getLogin())) return user;
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (User user : users)
            if (email.equals(user.getEmail())) return user;
        return null;
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        remove(user);
        return user;
    }

    @Override
    public User removeById(String id) {
        final User user = findById(id);
        remove(user);
        return user;
    }

}
