package ru.tsc.pavlov.tm.command.task;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_REMOVE_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!getTaskService().existsById(id)) throw new TaskNotFoundException();
        getTaskService().removeById(id);
        System.out.println("[OK]");
    }

}
