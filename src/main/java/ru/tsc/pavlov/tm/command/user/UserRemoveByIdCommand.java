package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand{
    @Override
    public String getName() {
        return TerminalConst.USER_REMOVE_BY_ID_COMMAND;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "User remove by Id";
    }

    @Override
    public void execute() {
        System.out.println("PLEASE ENTER USER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = getUserService().removeById(id);
        System.out.println("USER DELETED");
    }

}
