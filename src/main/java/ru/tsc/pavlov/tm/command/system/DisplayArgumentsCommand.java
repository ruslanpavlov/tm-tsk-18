package ru.tsc.pavlov.tm.command.system;

import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.constant.TerminalConst;


import java.util.Collection;

public class DisplayArgumentsCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.ARGUMENTS;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display list of arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            System.out.println(argument.getArgument());

        }
    }

}