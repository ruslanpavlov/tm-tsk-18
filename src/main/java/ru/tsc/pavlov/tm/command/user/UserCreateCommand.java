package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserCreateCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return TerminalConst.USER_CREATE_COMMAND;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Creating user";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER USER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER USER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSPHRASE:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER USER E-MAIL:");
        final String email = TerminalUtil.nextLine();

        final User user = getUserService().create(login, password, email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        showUser(user);
    }

}
