package ru.tsc.pavlov.tm.model;

import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.util.HashUtil;

import java.util.UUID;

public class User {

    private String id = UUID.randomUUID().toString();

    private String login;

    private String passwordHash;

    private String email;

    private UserRole role = UserRole.USER;

    private String firstName;

    private String lastName;

    private String middleName;

    public User() {

    }

    public User(String login, String password) {
        this.login = login;
        this.passwordHash = password;
    }

    public User(String login, String password, UserRole userRole) {
        this.login = login;
        this.passwordHash = password;
        this.role = userRole;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return passwordHash;
    }

    public void setPassword(String password) {
        this.passwordHash = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        return id + ": " + login;
    }

}
